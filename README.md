# Http Service

The project is to create a library for HTTP service. Other projects can use it to create HTTP connection easily.

- [Description](#description)
- [Usage](#usage)


## Description
DefaultHttpService is a class which you may use it for creating a HTTP call.
You may add more HTTP methods in HttpService and implement it in DefaultHttpService or you can inherit DefaultHttpService to build your own http service.

## Usage
Below is an example.

        HttpService service = new DefaultHttpService();
        try(CloseableHttpResponse response = service.executeGet(new URL("https://www.google.com"))){
        logger.info("status code:" + response.getStatusLine().getStatusCode());
        for(Header header: response.getAllHeaders()){
        logger.info(header.getName() + ": " + header.getValue());
        }
