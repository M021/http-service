package com.mlcodehk.http.exception;

public class HttpClientException extends Exception {
    public HttpClientException(){
        super();
    }
    public HttpClientException(String message){
        super(message);
    }

    public HttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

}