package com.mlcodehk.http.service;

import com.mlcodehk.http.exception.HttpClientException;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLInitializationException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;


public class HttpClientFactory {

    public static final int HTTP_CLIENT_MAX_TOTAL_CONNECTION = 200;

    public static final int HTTP_CLIENT_MAX_PER_ROUTE = 15;

    public static CloseableHttpClient getHttpClient() throws HttpClientException {
        return getHttpClient(HTTP_CLIENT_MAX_TOTAL_CONNECTION, HTTP_CLIENT_MAX_PER_ROUTE);
    }

    public static CloseableHttpClient getHttpClient(int maxTotalConnection, int maxPerRoute) throws HttpClientException {
        return getHttpClient(maxTotalConnection, maxPerRoute, null, null);
    }

    public static CloseableHttpClient getHttpClient(int maxTotalConnection, int maxPerRoute, String keystorePath, String password) throws HttpClientException {
        CloseableHttpClient httpClient;
            try {
                httpClient = createHttpClient(createSslContext(keystorePath, password),
                        maxTotalConnection, maxPerRoute);
            } catch (Exception e) {
                throw new HttpClientException(e.getMessage(), e.getCause());
            }
        return httpClient;
    }

    private static CloseableHttpClient createHttpClient(final SSLContext sslContext, int maxTotalConnection, int maxPerRoute) {
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext);
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", sslSocketFactory)
                        .build()
        );
        cm.setMaxTotal(maxTotalConnection);
        cm.setDefaultMaxPerRoute(maxPerRoute);
        return HttpClients.custom().setConnectionManager(cm).build();
    }

    private static SSLContext createSslContext(String keyStorePath, String password) throws CertificateException, IOException, KeyManagementException {
        final SSLContext sslContext;

        try {
            sslContext = SSLContext.getInstance("TLS");
            final TrustManagerFactory defaultTrustManager = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            final TrustManagerFactory customTrustManager = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());

            defaultTrustManager.init((KeyStore) null);
            customTrustManager.init(keyStorePath == null ? null : getKeyStore(keyStorePath, password));

            sslContext.init(null, new TrustManager[]{
                    new DelegateTrustManager((X509TrustManager) customTrustManager.getTrustManagers()[0], (X509TrustManager) defaultTrustManager.getTrustManagers()[0])
            }, new SecureRandom());

        } catch (NoSuchAlgorithmException e) {
            throw new SSLInitializationException(e.getMessage(), e);
        } catch (KeyStoreException e) {
            throw new SSLInitializationException(e.getMessage(), e);
        }
        return sslContext;
    }

    private static KeyStore getKeyStore(String keyStorePath, String password) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore ks = KeyStore.getInstance("JKS");
        try (InputStream is = new FileInputStream(keyStorePath)){
            ks.load(is, password.toCharArray());
        }
        return ks;
    }

}
