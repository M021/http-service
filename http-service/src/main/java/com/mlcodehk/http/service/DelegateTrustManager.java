package com.mlcodehk.http.service;

import org.apache.commons.lang3.ArrayUtils;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


public class DelegateTrustManager implements X509TrustManager {

    private final X509TrustManager trustManager;

    private final X509TrustManager defaultTrustManager;

    public DelegateTrustManager(X509TrustManager trustManager, X509TrustManager defaultTrustManager) {
        this.trustManager = trustManager;
        this.defaultTrustManager = defaultTrustManager;
    }

    public void checkClientTrusted(X509Certificate[] x509Certificates, String authType) throws CertificateException {
        try {
            trustManager.checkClientTrusted(x509Certificates, authType);
        } catch (Exception ignore) {
            defaultTrustManager.checkClientTrusted(x509Certificates, authType);
        }
    }

    public void checkServerTrusted(X509Certificate[] x509Certificates, String authType) throws CertificateException {
        try {
            trustManager.checkServerTrusted(x509Certificates, authType);
        } catch (Exception ignore) {
            defaultTrustManager.checkServerTrusted(x509Certificates, authType);
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        X509Certificate[] first = trustManager.getAcceptedIssuers();
        X509Certificate[] second = defaultTrustManager.getAcceptedIssuers();
        X509Certificate[] result = ArrayUtils.addAll(first, second);
        return result;
    }

}
