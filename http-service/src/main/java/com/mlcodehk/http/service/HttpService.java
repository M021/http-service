package com.mlcodehk.http.service;

import com.mlcodehk.http.exception.HttpServiceException;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.net.URL;
import java.util.Map;

public interface HttpService {

     CloseableHttpResponse executeGet(URL url) throws HttpServiceException;

     CloseableHttpResponse executeGet(URL url, Map<String, String> httpHeaders) throws HttpServiceException;
}
