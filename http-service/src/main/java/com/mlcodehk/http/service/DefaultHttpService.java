package com.mlcodehk.http.service;


import com.mlcodehk.http.exception.HttpServiceException;
import lombok.extern.slf4j.Slf4j;
import com.mlcodehk.http.exception.HttpClientException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;


import java.io.IOException;
import java.net.URL;
import java.util.Map;


@Slf4j
public class DefaultHttpService implements HttpService {

    protected CloseableHttpClient httpClient;

    public DefaultHttpService() throws HttpClientException {
        this.httpClient = HttpClientFactory.getHttpClient();
    }

    public DefaultHttpService(int maxTotalConnection, int maxPerRoute, String keystorePath, String password) throws HttpClientException {
        this.httpClient = HttpClientFactory.getHttpClient(maxTotalConnection, maxPerRoute, keystorePath, password);
    }

    //provide a shorthand setting, you can manually set it by your own.
    protected void setHeader(HttpRequestBase base) {
        base.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
    }

    public CloseableHttpResponse executeGet(URL url) throws HttpServiceException {
        return executeGet(url, null);
    }

    public CloseableHttpResponse executeGet(URL url, Map<String, String> httpHeaders) throws HttpServiceException {
        HttpGet httpGet = new HttpGet(url.toString());

        if (httpHeaders != null) {
            for (Map.Entry<String, String> entry : httpHeaders.entrySet()) {
                httpGet.setHeader(entry.getKey(), entry.getValue());
            }
        } else {
            setHeader(httpGet);
        }

        CloseableHttpResponse response = null;

        logger.debug("url : " + url.toString());
        try {
            response = httpClient.execute(httpGet);
        } catch (IOException e) {
            logger.error("Failed to get the response -  url: " + httpGet.getURI(), e.getCause());
            IOUtils.closeQuietly(response);
            throw new HttpServiceException(e.getMessage(), e.getCause());
        }

        try {
            validateStatusCode(response, httpGet.getURI().toString());
        } catch (Exception e) {
            IOUtils.closeQuietly(response);
            throw e;
        }

        if (response.getEntity() == null) {
            IOUtils.closeQuietly(response);
            throw new HttpServiceException("The entity is null - url: " + httpGet.getURI());
        }
        return response;
    }


    private void validateStatusCode(HttpResponse response, String url) throws HttpServiceException {
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            String errMsg = "The response status code is not valid. -  url: " + url + " statusCode: " + statusCode;
            throw new HttpServiceException(errMsg);
        }
    }


}
