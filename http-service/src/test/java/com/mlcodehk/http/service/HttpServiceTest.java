package com.mlcodehk.http.service;

import com.mlcodehk.http.exception.HttpClientException;
import com.mlcodehk.http.exception.HttpServiceException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpServiceTest {

    private final String DEFAULT_TESTING_URL =  "https://www.google.com/";

    @Test
    public void testGetMethod() throws HttpClientException, IOException, HttpServiceException {
        HttpService service = new DefaultHttpService();

        try(CloseableHttpResponse response = service.executeGet(new URL(DEFAULT_TESTING_URL))){
            Assert.assertEquals(200, response.getStatusLine().getStatusCode());
        }
    }


}
